## How to fix broken grub when you using zfs on root

Base of recepient I've got [here](https://www.reddit.com/r/zfs/comments/oy6d5h/comment/h7r3kj5/?utm_source=share&utm_medium=web2x&context=3)

    grub> 

    ls 
    (hd0,msdosX) or (hd0,gptX)

you need

    ls (hdX,msdosX)/ 

each of them and find where directoryes @/ and BOOT/

then 

    set root=(hd0,gptXXX)
    linux /BOOT/ubuntu_YYY/@/vmlinuz root=ZFS=rpool/ROOT/ubuntu_YYY boot=zfs
    initrd /BOOT/ubuntu_YYY/@/initrd.img
    boot

afte all 

    sudo update-grub
